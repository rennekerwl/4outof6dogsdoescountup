const fs = require('fs');
const wordListPath = require('word-list');
const wordArray = fs.readFileSync(wordListPath, 'utf8').split('\n');
var wordSet = new Set(wordArray);

console.log(wordArray.length);

var tree = function(leafs){
    var branches = [];
    if( leafs.length == 1) return leafs;
    for( var k in leafs ){
	var leaf = leafs[k];
	tree(leafs.join('').replace(leaf,'').split('')).concat("").map(function(subtree) {
	    branches.push([leaf].concat(subtree));
	});
    }
    return branches;
};

function uniq(a){
    var seen = {};
    return a.filter(function(item){
	return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

var word = 'etisirinc';
combos = tree(word.split('')).map(function(str){return str.join('')});
combosUnique = uniq(combos);
combosUnique.sort(function(a,b){
    return b.length - a.length;
});

var comboSet = new Set(combos);

for (var i = 0; i < combosUnique.length; i++){
    word = combosUnique[i];
    isWord = wordSet.has(word.toLowerCase());
    if (isWord === true){
	console.log('---------------------');
	console.log('FOUND A WORD: ' + word);
	break;
    }
    else{
	console.log(word + ' is not a word :(');
    }
}
