//Import word list library
const fs = require('fs');
const wordListPath = require('word-list');
const wordArray = fs.readFileSync(wordListPath, 'utf8').split('\n');
var wordSet = new Set(wordArray);
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
//var http = require('http').Server(app);
//var io = require('socket.io')(http);

// Database
var mongo = require('mongodb');
var monk = require('monk');
// var db = monk('localhost:27017/nodetest1');
var db = monk('localhost:27017/nodetest1');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var gamesRouter = require('./routes/games');

var app = express();

//experimental
var server = app.listen(3001);
var io = require('socket.io').listen(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Make our db accessible to our router
app.use(function(req, res, next) {
    req.db = db;
    next();
});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/games', gamesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


/*
 *Takes an array and filters out duplicates
 *so that each array element is unique
 *@param a array
 *@returns array without duplicate
 */
function uniq(a) {
    var seen = {};
    return a.filter(function(item) {
        return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}




//Used like combos = tree('fisherman'.split('').map(function(str){return str.join('')});
/*
 *Used for letter round to detrmine all combinations of the input string
 *@param leafs a string letters
 *@returns every permutation of that combination of letters
 */
var tree = function(leafs) {
    var branches = [];
    if (leafs.length == 1) return leafs;
    for (var k in leafs) {
        var leaf = leafs[k];
        tree(leafs.join('').replace(leaf, '').split('')).concat("").map(function(subtree) {
            branches.push([leaf].concat(subtree));
        });
    }
    return branches;
};




//solves anagrams -- unused
/*
function solveAnagram (anagram, data, start, end, currentIndex, wordSize){
    // Current combination matches wordsize 
    if (currentIndex == wordSize) 
    { 
        //check dictionary to see if data is a word in our dictionary
	if(wordArray.indexOf(data.toLowerCase())){
	    //Word is in dictionary, return the data
            return data;
        }
	else{
            return;
	} 
    } 
    
    // replace currentIndex with all possible elements. The condition 
    // "end-i+1 >= wordSize-currentIndex" makes sure that including one element 
    // at index will make a combination with remaining elements 
    // at remaining positions 
    for (i=start; i<=end && end-i+1 >= wordSize-currentIndex; i++) 
    { 
        data[currentIndex] = anagram[i]; 
        return solveAnagram(anagram, data, i+1, end, currentIndex+1, wordSize); 
    } 
}*/



// //Socket.IO stuff
io.on('connection', function(socket) {

    /*if receiving chat message, broadcast the chat message*/
    socket.on('chat message', function(msg, room) {
        io.to(room).emit('chat message', msg);
    });

    socket.on('join game', function(userName, room) {
        socket.join(room);
        io.to(room).emit('join game', userName, room);
    });

    socket.on('start round', function(room) {
        io.to(room).emit('start round');
    });

    socket.on('letter round', function(picker, room) {
        io.to(room).emit('letter round', picker);
    });

    socket.on('number round', function(picker, room) {
        io.to(room).emit('number round', picker);
    });

    socket.on('conundrum round', function(picker, room) {
        io.to(room).emit('conundrum round', picker);
    });

    socket.on('update lb', function(letter, position, room) {
        io.to(room).emit('update lb', letter, position);
    });

    socket.on('update cb', function(conundrumData, room) {
        io.to(room).emit('update cb', conundrumData);
    });

    socket.on('start letterTimer', function(room) {
        io.to(room).emit('start letterTimer');
    });

    socket.on('start conundrumTimer', function(room) {
        io.to(room).emit('start conundrumTimer');
    });

    //When a word check event is received send back (just to the user that sent the event) -1 if its not a word or the index if it is.
    /*
     *Checks to see if the word is in the list of possible words
     *@param word - string
     *@returns the array element of the word on the list OR -1 if the word is not found
     */

    socket.on('word check', function(word) {
        socket.emit('word answer', word, wordArray.indexOf(word.toLowerCase()));
    });

    socket.on('update workspace lb', function(scoreInfo, room) {
        io.to(room).emit('update workspace lb', scoreInfo);
    });

    socket.on('update workspace lb2', function(scoreInfo, room) {
        io.to(room).emit('update workspace lb2', scoreInfo);
    });

    socket.on('update workspace nb', function(scoreInfo, room) {
        io.to(room).emit('update workspace nb', scoreInfo);
    });

    socket.on('display lr results', function(room) {
        socket.emit('display lr results');
    });

    socket.on('display nr results', function(room) {
        socket.emit('display nr results');
    });

    socket.on('display cr results', function(winningUser, room) {
        io.to(room).emit('display cr results', winningUser);
    });

    socket.on('show final', function(room) {
        io.to(room).emit('show final');
    });

    socket.on('show nr solution', function(room) {
        socket.emit('show nr solution');
    });


    //This socket event takes in a string and returns the longest English word that can be found in that string to the user that emitted the event.
    //takes string of all letters on the board, gets every permutation of the string, remove duplicates, then sort by length and return the longest string
    /*
     *@param board string of all letters on the board
     *Sends longest word to the client that initiated the function
     */
    socket.on('get longest word', function(board) {
        combos = tree(board.split('')).map(function(str) {
            return str.join('')
        });
        combosUnique = uniq(combos);
        combosUnique.sort(function(a, b) {
            return b.length - a.length;
        });
        for (var i = 0; i < combosUnique.length; i++) {
            word = combosUnique[i];
            isWord = wordSet.has(word.toLowerCase());
            if (isWord === true) {
                socket.emit('found longest word', word);
                break;
            }
        }
    });

    socket.on('update nb', function(number, position, room) {
        io.to(room).emit('update nb', number, position);
    });

    socket.on('start numberTimer', function(room) {
        io.to(room).emit('start numberTimer');
    });

    socket.on('set target number', function(target, room) {
        io.to(room).emit('set target number', target);
    });
	
	socket.on('get word list', function(room){
		io.to(room).emit('get word list', wordArray);
	});

});

io.on('disconnect', function () {
    socket.removeAllListeners('send message');
    socket.removeAllListeners('disconnect');
    io.removeAllListeners('connection');
});

module.exports = app;
