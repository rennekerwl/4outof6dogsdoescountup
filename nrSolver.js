//
var target = 940;
var numbers = [50, 25, 2, 2, 1, 9];

/*function add(a, b){
    return a + b;
}

function sub(a, b){
    return a - b;
}

function mul(a, b){
    return a * b;
}

function div(a, b){
    return a / b;
}

numbers.sort(function (lhs, rhs) {return lhs - rhs;});
//First check if any of the numbers is equal to the target

function uniq(a){
    var seen = {};
    return a.filter(function(item){
	return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    });
}

//This generates a list of every possible combination of the input string.
//Used like combos = tree('fisherman'.split('').map(function(str){return str.join('')});
var tree = function(leafs){
    var branches = [];
    if( leafs.length == 1) return leafs;
    for( var k in leafs ){
	var leaf = leafs[k];
	tree(leafs.join('').replace(leaf,'').split('')).concat("").map(function(subtree) {
	    branches.push([leaf].concat(subtree));
	});
    }
    return branches;
};

var combine = function(a, min) {
    var fn = function(n, src, got, all) {
	if (n == 0) {
	    if (got.length > 0) {
		all[all.length] = got;
	    }
	    return;
	}
	for (var j = 0; j < src.length; j++) {
	    fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
	}
	return;
    }
    var all = [];
    for (var i = min; i < a.length; i++) {
	fn(i, a, [], all);
    }
    all.push(a);
    return all;
}

var subsets = combine(numbers, 1);
var subsetsUnique = uniq(subsets);
console.log(subsets);
console.log(subsets.length);
console.log(subsetsUnique.length);
console.log(add(1, 2));
console.log(sub(1, 2));
console.log(mul(1, 2));
console.log(div(1, 2));*/

//Solution starts here
function solve(numberList, targetSolution) {
    var operations = ["+", "-", "*", "/"]
    var solutions = [];
    var allPermsOfList = permute(numberList);
    allPermsOfList = removeDuplicates(allPermsOfList);
    allPermsOfList.forEach(function(list) {
	solveRecursive(list, list[0].toString())
    });
    return solutions;

    function solveRecursive(numberList, testExpression) {
	if (numberList.length == 1) {
	    if (approximatelyEqual(numberList[0], targetSolution)) {
		solutions.push(testExpression);
	    }
	} else {
	    for(var operatorNum = 0; operatorNum < operations.length; operatorNum ++) {
		var newExpression = testExpression + operations[operatorNum] + numberList[1].toString();
		var collapsedList = collapseE1E2(numberList, operations[operatorNum]);
		solveRecursive(collapsedList, newExpression);
	    }
	}

    }

    function collapseE1E2(numberList, operation) {
	var copyArray = numberList.slice();
	var combined = copyArray[0];
	switch(operation) {
	case "+":
	    combined += copyArray[1];
	    break;
	case "-":
	    combined -= copyArray[1];
	    break;
	case "*":
	    combined *= copyArray[1];
	    break;
	case "/":
	    combined /= copyArray[1];
	    break;
	default:
	    throw("Unknown operation encountered during combination attempt!");
	}
	copyArray.splice(0, 2, combined);
	return copyArray;
    }

    function approximatelyEqual(n1, n2) {
	if(Math.abs(n1 - n2 ) < 0.00001) { //ish? sure.
	    return true;
	}
    }

}

//Heap's Algorithm for Generating Permutations https://en.wikipedia.org/wiki/Heap%27s_algorithm
function permute(array) {

    var permutations = [];
    generate(array);
    return permutations;

    function generate (array, n) {
	n = n || array.length;
	if (n == 1) {
	    permutations.push(array.slice());
	} else {
	    for (var i = 0; i < n - 1; i ++) {
		generate(array, n - 1);
		if (n % 2 === 0) {
		    swap(array, i, n-1);
		} else {
		    swap(array, 0, n-1)
		}
	    }
	    generate(array, n-1);
	}
    }

    function swap (array, a, b) {
	var temp = array[a];
	array[a] = array[b];
	array[b] = temp;
    }
}

//taken from https://stackoverflow.com/questions/9229645/remove-duplicates-from-javascript-array
function removeDuplicates(array) {
    var seen = {};
    return array.filter(function(item) {
	return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    })
}

console.log(solve(numbers, target));
