
// Userlist data array for filling in info box
var userListData = [];
var gameListData = [];
// DOM Ready =============================================================
$(document).ready(function() {

    // Populate the user table on initial page load
    //populateTable();

    // Username link click
    //$('#userList table tbody').on('click', 'td a.linkshowuser', showUserInfo);

    // Add User button click
    //$('#btnAddUser').on('click', addUser);

    // Delete User link click
    //$('#userList table tbody').on('click', 'td a.linkdeleteuser', deleteUser);

    // Update User link click
    //$('#userList table tbody').on('click', 'td a.linkupdateuser', updateUser);

    // Make and show game key button click
    $('#btnGenerateKey').on('click', displayKey);

    // Add Create Game button click
    $('#btnCreateGame').on('click', addGame);

    // Add Join Game button click    
    $('#btnJoinGame').on('click', userJoinGame);

});

// Functions =============================================================

//Generate and display game key
function displayKey(event){
    event.preventDefault();
    var key = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = 0; i < 4; i++){
	key += possible.charAt(Math.floor(Math.random() * possible.length));
    };
    $('#keyspan').text(key);
};

//Generate a 4 character game key
function generateKey(){
    var key = '';
    var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    for (var i = 0; i < 4; i++){
	key += possible.charAt(Math.floor(Math.random() * possible.length));
    };
    $('head title').text(key + ' - ' + $('#joinGame form input#createGameUserName').val() + ' - ' + 'Team 1');

};

/*
 *Add game to DB - Used only during game creation
 *@param event - submit event
 *@returns nothing on success or false if error
*/
function addGame(event) {
    console.log('addGame is running');
    event.preventDefault();
    var username = $('#createGameUserName').val();
    var gameMode = $("input[name='numPlayersOptions']:checked").val();
    console.log(gameMode)

    if (username){
	// Super basic validation - increase errorCount variable if any fields are blank
	var errorCount = 0;
	/*$('#addGame input').each(function(index, val) {
	  if($(this).val() === '') { errorCount++; }
	  });*/
	
	// Check and make sure errorCount's still at zero
	if(errorCount === 0) {
	    setTimeout(function(){
		var key = $('head title').text().split(' - ')[0];
		console.log('addGame key is ' + key);
		// If it is, compile all user info into one object
		var newGame = {
		    'key': key,
		    'numPlayers': gameMode,
		    'users': [ $('#joinGame form input#createGameUserName').val() ],
		    'team1': [ $('#joinGame form input#createGameUserName').val() ],
		    //Tried adding empty team 2 here but it does work.  Must be added in userJoinGame.
		    //'team2': [],
		    'round': 0,
		    'score': [0, 0]
		}
		
		////Use this to update an array
		//newGame['staticArrayTest'].push('testing4');
		
		// Use AJAX to post the object to our adduser service
		$.ajax({
		    type: 'POST',
		    data: newGame,
		    url: '/games/addgame',
		    dataType: 'JSON'
		}).done(function( response ) {
		    
		    // Check for successful (blank) response
		    if (response.msg === '') {
			//Update display name area with username
			$('#displayName').text('Welcome ' + $('#joinGame form input#createGameUserName').val());
			$('#joinGame form input').val('');
		    }
		    else {
			// If something goes wrong, alert the error message that our service returned
			alert('Error: ' + response.msg);
		    }
		});
	    }, 500);
	}
	else {
	    // If errorCount is more than 0, error out
	    alert('Please fill in all fields');
	    return false;
	}
    }
    else{
	alert("Enter a username to continue.");
    }
};


//add user to game in db
/*
 *Add user to game in DB. Used when a user joins a game.  
 *@param event - form submission event
 *@returns - nothing
*/
function userJoinGame(event){
    event.preventDefault();
    console.log('userJoinGame() running');
    var noDuplicates = true;
    var gameToUpdateKey = $('#joinGame form input#inputGameKey').val().toUpperCase();
    var username = $('#joinGame form input#inputGameUserName').val();
	var userTeam;

    
    //Check for game key
    if (gameToUpdateKey){
	if (username){
	    //Do a GET request to pull current state of game in DB
	    // jQuery AJAX call for JSON
	    $.getJSON( '/games/games', function( data ) {
		
		// Stick our user data array into a userlist variable in the global object
		userListData = data;
		
		//Find the game in the array based on the key
		//var gameToUpdateKey = $('#joinGame form input#inputGameKey').val().toUpperCase();
		console.log(gameToUpdateKey);
		var gameToUpdate = data.find(function(element) {
		    return element['key'] === gameToUpdateKey;
		});

		//Add our username to the users[] array
		//If users is a string we need to make it into a list
		if (Object.prototype.toString.call(gameToUpdate['users[]']) === '[object String]') {
		    //Check for duplicate username
		    if ($('#joinGame form input#inputGameUserName').val().toUpperCase() != gameToUpdate['users[]'].toUpperCase()){
			//Add new user to the list
			var updatedUserList = [gameToUpdate['users[]'], $('#joinGame form input#inputGameUserName').val()];
			gameToUpdate['users[]'] = updatedUserList;
		    }
		    else{
			noDuplicates = false;
			alert("That username is already taken.  Please choose another.");
		    }
		}
		//Else if users is already a list we just append the new user to the list
		else {
		    //Check for duplicate username 
		    if (gameToUpdate['users[]'].indexOf($('#joinGame form input#inputGameUserName').val()) === -1){
			gameToUpdate['users[]'].push($('#joinGame form input#inputGameUserName').val());
		    }
		    else{
			noDuplicates = false;
			alert("That username is already taken.  Please choose another.");
		    }
		}

		//Add our username to the correct team array
		//Get team 1 object
		var team1 = gameToUpdate['team1[]'];
		//Check for team 2 object
		if (Object.keys(gameToUpdate).indexOf('team2[]') > -1){
		    var team2 = gameToUpdate['team2[]'];
		    //If team 2 object exists and team 1 object is a string, turn team 1 object into an array and add our username to it.
		    if (Object.prototype.toString.call(gameToUpdate['team1[]']) === '[object String]') {
			var updatedTeam1 = [gameToUpdate['team1[]'], username];
			gameToUpdate['team1[]'] = updatedTeam1;
			userTeam = 'Team 1';
		    }
		    //team 1 object is an array, check team 2 object
		    else{
			//If team 2 is a string, turn it into an array an add our username to it
			if (Object.prototype.toString.call(team2) === '[object String]') {
			    var updatedTeam2 = [gameToUpdate['team2[]'], username];
			    gameToUpdate['team2[]'] = updatedTeam2;
				userTeam = 'Team 2';
			}
			//If team 2 is an array, add our user to team 1 array if they are the same length, or team 2 array if team 1 array is longer.
			else{
			    if (team1.length === team2.length){
				gameToUpdate['team1[]'].push(username)
				userTeam = 'Team 1';
			    }
			    else{
				gameToUpdate['team2[]'].push(username)
				userTeam = 'Team 2';
			    }
			}
		    }
		}
		//If team 2 object doesn't exist, we must create it.
		else{
		    gameToUpdate['team2[]'] = [$('#joinGame form input#inputGameUserName').val()];
			userTeam = 'Team 2';
		}
		    
		//If the username is unique then send updated game obj to DB
		if (noDuplicates){
		    //Send updated game object to the DB
		    $.ajax({
			type: 'PUT',
			data: gameToUpdate,
			url: '/games/putgame/' + gameToUpdate['_id']
		    }).done(function( response ) {
			//Check for a successful (blank) response
			if (response.msg === ''){
			    
			    // Store key for populateLobby function and Clear the form inputs
			    var key = $('#joinGame form input#inputGameKey').val().toUpperCase();
			    
			    //Set the tab title to the game key and the username
				if (userTeam === 'Team 1'){
			    $('head title').text(key + ' - ' + $('#joinGame form input#inputGameUserName').val() + ' - Team 1');
			    }
				else {
					$('head title').text(key + ' - ' + $('#joinGame form input#inputGameUserName').val() + ' - Team 2');
				}
			    $('#joinGame form input').val('');
			}
			else {
			    alert('Error: ' + response.msg);
			}
		    });
		}
		else{
		    $('#joinGame form input#inputGameUserName').val('');
		}
	    });
	}
	else{
	    alert("Enter a username to continue.");
	}
    }
    else{
	alert("Enter a game key to continue.");
    }
};


//Return JSON object with data for a particular game
/*
 *@param gameKey - 4 letter string used to join games
 *@returns JSON object with everything stored in our DB related to the game
*/
function getGameData(gameKey){
    //make our ajax call wait for data before proceeding
    $.ajaxSetup({
	async: false
    });

    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our game data array into a variable
	gameData = data;

	// For each item in our array, if it has a key and the key matches, return it.
	$.each(data, function(){
	    if ('key' in this && this['key'] === gameKey) {

		workingCopy = this;
		console.log("workingCopy: " + JSON.stringify(workingCopy));
		return workingCopy;
	    }
	});
    });
};


// Fill lobby table with data
/*
 *Fill loby with user names when game is created or updated.  Responsible for setting up and updating the lobby.
 *@param key - game key
 *@returns nothing
*/
function populateLobby(key) {
    // Empty content string
    var lobbyContent = '';
    var team1Content = '';
    var team2Content = '';
    console.log('populateLobby function running!');
	
    lobbyContent += "<p>Go head to head against another player in a game of math and word-skill. See who is the brightest in a 5 round game.</p>";

    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && 'users[]' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
		gameContent = workingCopy;
		//If workingCopy['users[]'] is a string, just add the string to lobbyContent. This only happens when there is one player
		if (workingCopy['users[]'].constructor.name === 'String') {
		    team1Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]']) + '.jpg"><div>' + workingCopy['users[]'] + '</div></div>';////////////this used to be a p tag
		}
		//If workingCopy['users[]'] is a list, add each item to lobbyContent
		if (workingCopy['users[]'].constructor.name === 'Array') {
		    for (var i = 0; i < workingCopy['users[]'].length; i++) {
					
			//even players go to team 1
			if(i % 2 === 0) {
			    team1Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';////////////this used to be a p tag
			}
			else{
			    team2Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';////////////this used to be a p tag
			}
		    }
		}
	    }
	});
	
	//add users to teams
	lobbyContent += "<div id='team1' class='col-md-3' style='float: left;'><p style='font-size: 3em; color: white; font-weight: bold;'>TEAM 1</p>" + team1Content + "</div><div id='team2' class='col-md-3' style='float: right;'><p style='font-size: 3em; color: white; font-weight: bold;'>TEAM 2</p>"+ team2Content +"</div>";
	
	//Clear wrapper div and create/display lobby.  Inject the content string into the HTML span.
	$('#wrapper').empty();
	$('#wrapper').append('<div id="gameLobbyShowKey" class="key"></div>');
	$('#wrapper').append('<div id="gameLobbyDiv" class="container" style="margin: auto;"></div>');
	$('#gameLobbyDiv').html(lobbyContent);
	$('#gameLobbyShowKey').text("Key: " + key);
	if (workingCopy['users[]'].constructor.name === 'Array' && workingCopy['users[]'].length === 2) {
	    console.log(gameContent['numPlayers']);
	    $('#wrapper').append('<form id="startGameForm" action=""><button type="button" id="btnStartGame" class="btn btn-primary">Start Game</button></form>');
	}
    });
};

//return random image url from array of image urls
function randomUserImage(userName){
	var nameNumber = 0;
	console.log("user name is " + userName);
	
	for(var i = 0; i < userName.length; i++){
		nameNumber += userName.charCodeAt(i) - 97;
	}
	
	console.log("Number is: " + nameNumber);
	
	nameNumber = nameNumber % 9;
	
	console.log("after modulo " + nameNumber);
	
    var icons=[
	"user1",
	"user2",
	"user3",
	"user4",
	"user5",
	"user6",
	"user8",
	"user9",
    ];
	
	console.log("Icon is " + icons[nameNumber]);
	
    //return icons[Math.floor(Math.random()*icons.length)];
	return icons[nameNumber];
}


/*
 *Activates when someone presses the start game button
 *Checks the number of users and ensures there are at least 2
 *Clears lobby screen
*/
function startGame(){
    console.log("StartGame function running");
	
    var key = $("#gameLobbyShowKey").text();
    if (key){
	console.log("Key: " + key);
    }
    else {
	var key = $('head title').text().split(' - ')[0];
    }

    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
				
		console.log("Number of users: " + workingCopy['users[]'].length);
				
		//If workingCopy['users[]'] is a list, check that there are at least two users
		//TEMPORARY: until teams are added, only allow 2 players
		if ((workingCopy['users[]'].constructor.name === 'Array') && workingCopy['users[]'].length >= 2 && workingCopy['users[]'].length == 2) {
		    //start game
		    $('#wrapper').empty();
		}
	    }
	});
    });
};


/*
 *Second attempt at a random letter generator.  This function returns a random vowel if letterType is 'vowel' and a random consonant if letterType is 'consonant'.
 *letters have a distribution which sets their probability of being chosen
 *@param letterType - "consonant" or "vowel"
 *@returns characterSet
*/
function getRandomLetter(letterType){
    if (letterType === 'vowel'){
	letter = $('#vowelCards').text()[Math.floor(Math.random() * $('#vowelCards').text().length)];
	$('#vowelCards').text($('#vowelCards').text().replace(letter, ''));
	return letter;
    }
    if (letterType === 'consonant'){
	letter = $('#consonantCards').text()[Math.floor(Math.random() * $('#consonantCards').text().length)];
	$('#consonantCards').text($('#consonantCards').text().replace(letter, ''));
	return letter;
    }
};


/*This function is for the letter round.  It takes the argument picker to determine which player picks the letters.
 *Draws board and scoreboard for letter round.
 *Draws consonant and vowel buttons for the picker
 *@param picker - array element for the user who gets to pick (stored in user array)
 *@returns nothing
*/
function letterRound(picker){
    $('#workSpace').empty();
    $('#gameMessages').empty();
    //Get the gameKey and the current user from the tab title
    var key = $('head title').text().split(' - ')[0];
    var currentUser = $('head title').text().split(' - ')[1];

    //Get the current state of the game object from the DB
    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
	    }
	});
    });

    //Setup the game board
    //Board is a table within letterBoard div, table cells are named cell0, cell1, etc through cell17
    //Form is letterRoundForm with btnChooseVowel, btnChooseConsonant, letterRoundSubmit buttons and letterRoundAnswer text field
    var letterRoundPage = '<div id="lrHTML">\
<p id="vowelCards" hidden>AAAAAAAAAAAAAAAEEEEEEEEEEEEEEEEEEEEEIIIIIIIIIIIIIOOOOOOOOOOOOOUUUUU</p>\
<p id="consonantCards" hidden>BBCCCDDDDDFFGGGHHJKLLLLLMMMMNNNNNNNNPPPPQRRRRRRRRRSSSSSSSSSTTTTTTTTTVWXYZ</p>\
<h2>LETTER ROUND</h2>\
<div id="letterBoard">\
<table class="lbTable" id="lbTable">\
<tr>\
<td class="emptyCell" id="cell0"> </td>\
<td class="emptyCell" id="cell1"> </td>\
<td class="emptyCell" id="cell2"> </td>\
<td class="emptyCell" id="cell3"> </td>\
<td class="emptyCell" id="cell4"> </td>\
<td class="emptyCell" id="cell5"> </td>\
<td class="emptyCell" id="cell6"> </td>\
<td class="emptyCell" id="cell7"> </td>\
<td class="emptyCell" id="cell8"> </td>\
</tr>\
<tr>\
<td class="emptyCell" id="cell9"> </td>\
<td class="emptyCell" id="cell10"> </td>\
<td class="emptyCell" id="cell11"> </td>\
<td class="emptyCell" id="cell12"> </td>\
<td class="emptyCell" id="cell13"> </td>\
<td class="emptyCell" id="cell14"> </td>\
<td class="emptyCell" id="cell15"> </td>\
<td class="emptyCell" id="cell16"> </td>\
<td class="emptyCell" id="cell17"> </td>\
</tr>\
</table>\
</div>\
<div id="letterBoardMessage">Use the letters on the board to spell the longest word you can in 30 seconds.  Each letter may only be used once.</div>\
<form id="letterRoundForm" action="">\
<div id="pickerButtons"></div><br>\
<input id="letterRoundAnswer" class="form-control col-md-6" style="margin: auto;" type="text" placeholder="Enter Your Answer">\
</form>\
</div>\
<div id="gameMessages"></div>';
    $('#wrapper').empty();
    $('#wrapper').append(letterRoundPage);
    //Add the scoreboard section to the page
    scoreBoard = '<div id="team1" class="col-md-3" style="float: left;"><div id="score0" style="font-size: 3em; color: white; font-weight: bold;">TEAM 1</div></div><div id="team2" class="col-md-3" style="float: right;"><div id="score1" style="font-size: 3em; color: white; font-weight: bold;">TEAM 2</div></div>';
    $('#wrapper').append(scoreBoard);
    team1Content = '';
    team2Content = '';
    $('#score0').append(" Score: " + workingCopy["score[]"][0]);
    $('#score1').append(" Score: " + workingCopy["score[]"][1]);
    for (var i = 0; i < workingCopy['users[]'].length; i++){
	if (i % 2 === 0){
	    team1Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
	else {
	    team2Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
    }
    $('#team1').append(team1Content);
    $('#team2').append(team2Content);

    //Add consonant and vowel buttons only for the picker.
    if (workingCopy['users[]'][picker] === currentUser){
	$('#pickerButtons').append('<p>Choose how many vowels and consonants you would like</p>');
	$('#pickerButtons').append('<button type="button" id="btnChooseVowel" class="btn btn-primary">Vowel</button><button type="button" id="btnChooseConsonant" class="btn btn-primary">Consonant</button>');
    }
    $('#workSpace').empty();
	
};


/*
 *This function takes a letter (String) and a position (String or Integer) as arguments.  
 *It sets the cell in that position to the given letter.
*/
function updateLetterBoard(letter, position){
    if ($('#cell' + position.toString())){
	$('#cell' + position.toString()).text(letter);
	$('#cell' + position.toString()).removeClass('emptyCell');
	$('#cell' + position.toString()).addClass('fullCell');
    }
};


/*
 *This function returns the leftmost empty square of the top row of the letter board.
 *@returns array index of first empty letter board square
*/
function getEmptySquares(){
    var board = $('#lbTable').text();
    return board.split(' ')[0].length;
};


//This is the function for the conundrum round.  Not implimented yet.
function conundrumRound(picker){
    $('#workSpace').empty();
    $('#gameMessages').empty();
    //Get the gameKey and the current user from the tab title
    var key = $('head title').text().split(' - ')[0];
    var currentUser = $('head title').text().split(' - ')[1];

    //Get the current state of the game object from the DB
    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
	    }
	});
    });

    //Setup the game board
    //Board is a table within letterBoard div, table cells are named cell0, cell1, etc through cell17
    //Form is letterRoundForm with btnChooseVowel, btnChooseConsonant, letterRoundSubmit buttons and letterRoundAnswer text field
    conundrumRoundPage = '<div id="crHTML">\
<h2>CONUNDRUM ROUND</h2>\
<div id"conundrumBoard">\
<table class=cbTable" id="cbTable">\
<tr>\
<td class="emptyCell" id="cell0"> </td>\
<td class="emptyCell" id="cell1"> </td>\
<td class="emptyCell" id="cell2"> </td>\
<td class="emptyCell" id="cell3"> </td>\
<td class="emptyCell" id="cell4"> </td>\
<td class="emptyCell" id="cell5"> </td>\
<td class="emptyCell" id="cell6"> </td>\
<td class="emptyCell" id="cell7"> </td>\
<td class="emptyCell" id="cell8"> </td>\
</tr>\
</table>\
</div>\
<div id="conundrumBoardMessage">Unscramble the letters on the board into a nine letter word.</div>\
<form id="conundrumRoundForm" action="">\
<div id="pickerButtons"></div><br>\
<input class="form-control col-md-6" id="conundrumRoundAnswer" style="margin: auto;" type="text" placeholder="Enter Your Answer">\
<input class="btn btn-primary" id="btnConundrumSubmit" type="submit" disabled>\
</form>\
<div id="gameMessages"></div>\
</div>';
    $('#wrapper').empty();
    $('#wrapper').append(conundrumRoundPage);
    //Add the scoreboard section to the page
    scoreBoard = '<div id="team1" class="col-md-3" style="float: left;"><div id="score0" style="font-size: 3em; color: white; font-weight: bold;">TEAM 1</div></div><div id="team2" class="col-md-3" style="float: right;"><div id="score1" style="font-size: 3em; color: white; font-weight: bold;">TEAM 2</div></div>';
    $('#wrapper').append(scoreBoard);
    team1Content = '';
    team2Content = '';
    $('#score0').append(" Score: " + workingCopy["score[]"][0]);
    $('#score1').append(" Score: " + workingCopy["score[]"][1]);
    for (var i = 0; i < workingCopy['users[]'].length; i++){
	if (i % 2 === 0){
	    team1Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
	else {
	    team2Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
    }
    $('#team1').append(team1Content);
    $('#team2').append(team2Content);

    /*scoreBoard = '<br><br><div id="scoreBoard"><table class="scoreTable" id="scoreTable"><tr><td class="scoreCell" id="score0">0</td><td class="scoreCell" id="score1">0</td></tr><tr><td class="nameCell" id="name0"> </td><td class="nameCell" id="name1"> </td></tr></table></div>'
    $('#wrapper').append(scoreBoard);
    //Set the scoreboard to the current score
    $('#score0').text(workingCopy["score[]"][0]);
    $('#score1').text(workingCopy["score[]"][1]);

    //add the names of the users to the scoreboard.  ONLY WORKS WITH 2 USERS.
    for (var i = 0; i < workingCopy['users[]'].length; i++){
	$('#name' + i).text(workingCopy['users[]'][i].toUpperCase());
    }*/
    
    //Add consonant and vowel buttons only for the picker.
    if (workingCopy['users[]'][picker] === currentUser){
	$('#pickerButtons').append('<p>Click "Start" to start the round.</p>');
	$('#pickerButtons').append('<button class="btn btn-primary" type="button" id="btnStartTimer">Start</button>');
    }
    $('#workSpace').empty();

};


//given a conundrum json dataset, writes the conundrum to the page
function drawConundrum(JSON){
    console.log('running drawConundrum');
    $('#pickerButtons').remove();
	
    scrambled = JSON.scrambled;
    unscrambled = JSON.unscrambled;
    $('#workSpace').append(unscrambled);
	
    for(var pos = 0; pos < 9; pos++){
	updateLetterBoard(scrambled[pos], pos);
	console.log("update CONUNDRUM board " + scrambled[pos]+ pos);
    }
};


//grabs a conundrum from the conundrums.json file and picks a random conundrum
//returns conundrum as JSON
function getConundrum(){
    return new Promise(function(resolve, reject){
	$.getJSON( 'conundrums.json', function(result){
	    var random = Math.floor(Math.random() * result.conundrums.length);
	    var length = result.conundrums.length;
	    var unscrambled = result.conundrums[random].unscrambled;
	    var scrambled = result.conundrums[random].scrambled;
	    
	    console.log("Unscrambled: " + unscrambled + " Scrambled: " + scrambled);
	    resolve(result.conundrums[random]);
	});
    });
};

/*This function is for the letter round.  It takes the argument picker to determine which player picks the letters.
 *Draws board and scoreboard for letter round.
 *Draws consonant and vowel buttons for the picker
 *@param picker - array element for the user who gets to pick (stored in user array)
 *@returns nothing
*/
function resultsRound(picker){
    $('#workSpace').empty();
    $('#gameMessages').empty();

    //Get the gameKey and the current user from the tab title
    var key = $('head title').text().split(' - ')[0];
    var currentUser = $('head title').text().split(' - ')[1];

    //Get the current state of the game object from the DB
    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
	    }
	});
    });

    //Setup the game board
    //Board is a table within letterBoard div, table cells are named cell0, cell1, etc through cell17
    //Form is letterRoundForm with btnChooseVowel, btnChooseConsonant, letterRoundSubmit buttons and letterRoundAnswer text field
    resultsRoundPage = '<div id="rrHTML">\
<h2>FINAL RESULTS</h2>\
<p>GAME OVER!</p>\
<div id="gameMessages"></div>';
    $('#wrapper').empty();
    $('#wrapper').append(resultsRoundPage);
    //Add the scoreboard section to the page
    scoreBoard = '<br><br><div id="scoreBoard"><table class="scoreTable" id="scoreTable"><tr><td class="scoreCell" id="score0">0</td><td class="scoreCell" id="score1">0</td></tr><tr><td class="nameCell" id="name0"> </td><td class="nameCell" id="name1"> </td></tr></table></div>'
    $('#wrapper').append(scoreBoard);
    //Set the scoreboard to the current score
    $('#score0').text(workingCopy["score[]"][0]);
    $('#score1').text(workingCopy["score[]"][1]);
    //add the names of the users to the scoreboard.  ONLY WORKS WITH 2 USERS.
    /*for (var i = 0; i < workingCopy['users[]'].length; i++){
	$('#name' + i).text(workingCopy['users[]'][i].toUpperCase());
	//Add the scores of each player to the page
	$('#gameMessages').append(workingCopy['users[]'][i] + ' ended the game with ' + workingCopy['score[]'][i] + ' points.<br>');
	}*/

    //Get the current game from DB.  Then add messages with current score for each user.  ONLY WORKS WITH 2 USERS.
    getGames().then(function(games){
	currentGame = games.find(function(element) {
	    return element['key'] === key;
	});

	$('#gameMessages').append(currentGame['users[]'][0] + ' ended the game with ' + currentGame['score[]'][0] + ' points.<br>');
	$('#gameMessages').append(currentGame['users[]'][1] + ' ended the game with ' + currentGame['score[]'][1] + ' points.<br>');

	$('#scoreBoard').remove();
    });

	
    
    $('#workSpace').empty();



	
};


//This function returns an array of all games in the DB (with all of the data).  Uses promises so it gets called weird.
function getGames(){
    return new Promise(function(resolve, reject){
	$.getJSON( '/games/games', function(result){
	    resolve(result)
		});
    });
};


//This function takes a key and an array of games and returns the game from the array with the matching key
//Called when updating checking or updating game state
function getSpecificGame(key, gameArray){
    for (i=0; i < gameArray.length; i++){
	if ('key' in gameArray[i] && gameArray[i]['key'] === key){
	    return gameArray[i];
	}
    }
};


//This function takes an array of games and returns how many rounds are in the current game from that array
/*
 *@param games array of all games, which we got from getGames function
 *@returns round number as integer
*/
function getRound(games){
    var key = $('head title').text().split(' - ')[0];
    var game = getSpecificGame(key, games);
    return game['round'];
};


//This function takes an array of all games from getGames().then() and a username as inputs.  It returns a string 1 if the user is in team 1 or a 2 if team 2.
function getTeam(games){
    var key = $('head title').text().split(' - ')[0];
    var game = getSpecificGame(key, games);
    var username = $('head title').text().split(' - ')[1];
	console.log("Running getTeam for game " + game + " and user " + username);
    if (game['team1[]'].constructor.name === 'String') {
	if (game['team1[]'] === username){
	    return '1';
	}
    }
    else {
	if (game['team1[]'].indexOf(username) >= 0) {
	    return '1';
	}
    }
    if (game['team2[]'].constructor.name === 'String') {
	if (game['team2[]'] === username){
	    return '2';
	}
    }
    else {
	if (game['team2[]'].indexOf(username) >= 0) {
	    return '2';
	}
    }
};
    


//This function hides the buttons to add random vowels and consonants.  It should be run after 9 letters have been selected.
//Also used for number round
function hideLetterButtons(){
    $('#pickerButtons').empty();
};


//This function takes a word as input.  It returns true if the word only uses letters from the board and returns false if it uses other letters.  Each letter can only be used once.
function checkWordToBoard(word){
    board = $('#lbTable').text();
    board = board.split('');
    word = word.split('');
    //For each letter in the word
    for (i = 0; i < word.length; i++){
	letter = word[i];
	//if that letter is on the board, remove it from the board and move on
	if (board.indexOf(letter) >= 0){
	    board.splice(board.indexOf(letter), 1);
	}
	//if it's not on the board return false
	else {
	    return false;
	}
    }
    //If all letters were on the board return true
    return true;
};


//This function counts how many vowels are on the letter board.
function countVowels(){
    board = $('#lbTable').text().trim();
    vowels = board.match(/[aeiou]/gi);
    if (vowels != null){
	return vowels.length;
    }
    else{
	return 0;
    }
};


//This function counts how many consonants are on the letter board.
function countConsonants(){
    board = $('#lbTable').text().trim();
    consonants = board.match(/[bcdfghjklmnpqrstvwxy]/gi);
    if (consonants != null){
	return consonants.length;
    }
    else{
	return 0
    }
};

    
//This function returns the letters on the top of the board on a LR
function getBoardLR(){
    board = $('#lbTable').text();
    return board.trim();
};


//This function returns the letters on the top of the board on a LR
function getBoardNR(){
    board = [parseInt($('#cell0').text(), 10), parseInt($('#cell1').text(), 10), parseInt($('#cell2').text(), 10), parseInt($('#cell3').text(), 10), parseInt($('#cell4').text(), 10), parseInt($('#cell5').text(), 10)];
    return board;
};


//This function updates the given users score in the DB by the given amount.  It takes a username (STRING) and score (INTEGER) as inputs.  Only works with 2 users.
//This function was replaced by updateScore2
function updateScore(user, score){
    //Get the game key from the title
    var key = $('head title').text().split(' - ')[0];
    var gameToUpdate = 'Not found yet';
    console.log('update score function running');
    //Get the game data from the API, copy it to a local JSON object, and update that object's score
    $.getJSON('/games/games', function(data){
	console.log('inside first ajax call');
	gameToUpdate = data.find(function(element) {
	    return element['key'] === key;
	});
	console.log('game to update: ' + JSON.stringify(gameToUpdate));
	var userToUpdate = gameToUpdate['users[]'].indexOf(user);
	var oldScores = gameToUpdate['score[]'];
	//This bit is specific to two users
	var updatedScores = [parseInt(oldScores[0], 10), parseInt(oldScores[1], 10)];
	updatedScores[userToUpdate] += score;
	//This bit is also specific to two users
	updatedScores[0] = updatedScores[0].toString();
	updatedScores[1] = updatedScores[1].toString();
	gameToUpdate['score[]'] = updatedScores;
	console.log('updated game: ' + JSON.stringify(gameToUpdate));
	$.ajax({
	    type: 'PUT',
	    data: gameToUpdate,
	    url: '/games/putgame/' + gameToUpdate['_id']
	}).done(function( response ) {
	    console.log('inside .done for second ajax call');
	    if (response.msg === ''){
		console.log('second ajax call finished successfully');
		//When finished do nothing?
	    }
	    else{
		alert('Error: ' + response.msg);
	    }
	});

    });
};


//This is a second attempt at an update score function that uses the getGames function instead of an AJAX call.
//This function updates the given users score in the DB by the given amount.  It takes a username (STRING) and score (INTEGER) as inputs.  Only works with 2 users.
function updateScore2(user, score){
    return new Promise(function(resolve, reject){
	//Get the game key from the title
	var key = $('head title').text().split(' - ')[0];
	var gameToUpdate;
	getGames().then(function(games){
	    gameToUpdate = games.find(function(element) {
		return element['key'] === key;
	    });

	    console.log('game to update: ' + JSON.stringify(gameToUpdate));
	    var userToUpdate = gameToUpdate['users[]'].indexOf(user);
	    var oldScores = gameToUpdate['score[]'];
	    //This bit is specific to two users
	    var updatedScores = [parseInt(oldScores[0], 10), parseInt(oldScores[1], 10)];
	    updatedScores[userToUpdate] += score;
	    //This bit is also specific to two users
	    updatedScores[0] = updatedScores[0].toString();
	    updatedScores[1] = updatedScores[1].toString();
	    gameToUpdate['score[]'] = updatedScores;
	    console.log('updated game: ' + JSON.stringify(gameToUpdate));
	    $.ajax({
		type: 'PUT',
		data: gameToUpdate,
		url: '/games/putgame/' + gameToUpdate['_id']
	    }).done(function( response ) {
		if (response.msg === ''){
		    console.log('second ajax call for tie score update finished successfully');
		    //When finished do nothing?
		    resolve(response);
		}
		else{
		    alert('Error: ' + response.msg);
		    reject(response);
		}
	    });
	});
    });
};


//This function updates the score for both teams in the current game by the score amount (INTEGER).
//this function exists because it was overwriting the score for the other user
//Only works with two users
function updateScoreTie(score){
    return new Promise(function(resolve, reject){
	//Get the game key from the title
	var key = $('head title').text().split(' - ')[0];
	var gameToUpdate;
	getGames().then(function(games){
	    gameToUpdate = games.find(function(element) {
		return element['key'] === key;
	    });

	    console.log('game to update: ' + JSON.stringify(gameToUpdate));
	    var oldScores = gameToUpdate['score[]'];
	    //This bit is specific to two users
	    var updatedScores = [parseInt(oldScores[0], 10), parseInt(oldScores[1], 10)];
	    updatedScores[0] += score;
	    updatedScores[1] += score;
	    //This bit is also specific to two users
	    updatedScores[0] = updatedScores[0].toString();
	    updatedScores[1] = updatedScores[1].toString();
	    gameToUpdate['score[]'] = updatedScores;
	    console.log('updated game: ' + JSON.stringify(gameToUpdate));
	    $.ajax({
		type: 'PUT',
		data: gameToUpdate,
		url: '/games/putgame/' + gameToUpdate['_id']
	    }).done(function( response ) {
		if (response.msg === ''){
		    console.log('second ajax call for tie score update finished successfully');
		    //When finished do nothing?
		    resolve(response);
		}
		else{
		    alert('Error: ' + response.msg);
		    reject(response);
		}
	    });
	});
    });
};


//This function updates a given user's score in the scoreboard by a given amount
//Not impliment yet!
function displayScore(user, score){
    var user = $('head title').text().split(' - ')[1];
}


//This function checks to see who won the letter round
//@returns string username - word - score
function highestScoreLR(){
    //Answer info is in the hiddenworkSpace paragraph.  Remove the first and last '.'
    var highest = ['Noone', 'No Answer', 0, 'Team'];
    var scoresInfo = $('#workSpace').text().slice(1,-1); //"T2P2-TITN-0-Team 2..T2P1-NIT-3-Team 2..test-TWINJAN-0-Team 1..T1P1-WINY-4-Team 1"
    var scoresInfoByUser = scoresInfo.split('.'); //["T2P2-TITN-0-Team 2", "", "T2P1-NIT-3-Team 2", "", "test-TWINJAN-0-Team 1", "", "T1P1-WINY-4-Team 1"]
	
	//remove blank elements from scoresInfoByUser array
	var filtered = scoresInfoByUser.filter(function (el) {return el != "";});
	
	scoresInfoByUser = filtered;
	
    for (i = 0; i < scoresInfoByUser.length; i++){
		var userInfo = scoresInfoByUser[i].split('-');
		userInfo[2] = parseInt(userInfo[2], 10);
		if (userInfo[2] > highest[2]){
			highest = userInfo;
		}
    }
    return highest[0] + '-' + highest[1] + '-' + highest[2].toString() + '-' + highest[3];
};
	

//This function returns an array with all users' score infos
//score info is name - word - length of word
function getScoresFromScoreboardLR(){
    scoreInfo = $('#workSpace').text().split('.').filter(Boolean);
    return scoreInfo;
};


//This function returns an array with all users' score infos
//score info is name - word - length of word
function getScoresFromScoreboardNR(){
    scoreInfo = $('#workSpace').text().split('.').filter(Boolean);
    return scoreInfo;
};


//This function checks for a tie in the letter round results.  Only works if there's 2 players.
//Returns true if a tie or false if no tie
function checkTieLR(){
    /*var scoresInfo = $('#workSpace').text().slice(1,-1);
    var scoresInfoByUser = scoresInfo.split('.');
    if (parseInt(scoresInfoByUser[1].split('-')[2], 10) === parseInt(scoresInfoByUser[3].split('-')[2], 10)){*/
    scores = getScoresFromScoreboardLR();
    if (scores[0].split('-')[2] === scores[1].split('-')[2]){
	return true;
    }
    else{
	return false;
    }
};


//This function checks if any answers were submitted by seeing if any characters are in the #workSpace
//Returns true if there were submissions and false if there were not
function checkForAnswersLR(){
    scoreInfo = $('#workSpace').text().slice(1,-1);
    if (scoreInfo === ''){
	return false;
    }
    else{
	return true;
    }
};


//This function get's a user's own score info (user, word, and potential score) for a LR
//returns string user - word - score
function getUserScoreInfoLR(){
    var user = $('head title').text().split(' - ')[1];
    var scoresInfo = $('#workSpace').text().slice(1,-1);
    var scoresInfoByUser = scoresInfo.split('.');
    var userScore;
    for (var i = 0; i < scoresInfoByUser.length; i++ ){
	if (scoresInfoByUser[i].split('-')[0] === user){
	    return scoresInfoByUser[i];
	}
    }
};


//This function increases the current game's round by 1
//get the current state of the game
//update round information
//update game satate
//uses promises
function incrementRound(){
    return new Promise(function(resolve, reject){
		console.log("beginning of promise");
		//Get the game key from the title
		var key = $('head title').text().split(' - ')[0];
		var gameToUpdate;
		getGames().then(function(games){
			gameToUpdate = games.find(function(element) {
			return element['key'] === key;
			});

			console.log('game to update (round): ' + JSON.stringify(gameToUpdate));
			var oldRound = gameToUpdate['round'];
			//This bit is specific to two users
			var updatedRound = parseInt(oldRound, 10) + 1;
			gameToUpdate['round'] = updatedRound;
			console.log('updated round for game: ' + JSON.stringify(gameToUpdate));
			$.ajax({
			type: 'PUT',
			data: gameToUpdate,
			url: '/games/putgame/' + gameToUpdate['_id']
			}).done(function( response ) {
			if (response.msg === ''){
				console.log('second ajax call for round update finished successfully');
				//When finished do nothing?
				resolve(response);
			}
			else{
				alert('Error: ' + response.msg);
				reject(response);
			}
			});
		});
		console.log("end of promise");
    });
};


//This function is for the number round.  It takes the argument picker to determine which player picks the numbers.

/*
 *@param picker - array index of user who will be picking
 *Draws number round buttons and scoreBoard
*/
function numberRound(picker){
	console.log("running numberRound");
    $('#workSpace').empty();
    $('#gameMessages').empty();
    //Get the gameKey and the current user from the tab title
    var key = $('head title').text().split(' - ')[0];
    var currentUser = $('head title').text().split(' - ')[1];
	
	//console.log('current user is ' + currentUser);

    //Get the current state of the game object from the DB
    // jQuery AJAX call for JSON
    $.getJSON( '/games/games', function( data ) {

	// Stick our user data array into a userlist variable in the global object
	gameData = data;

	// For each item in our JSON, if it has a key and has users and the key matches, add each user to the content string.
	$.each(data, function(){
	    if ('key' in this && this['key'] === key) {
		//this['users[]'] is a string if the game is being created or a list if it already exists
		workingCopy = this;
	    }
	});
    });

    //Setup the game board
    //Board is a table, rest is TBD
    //Form is TBD
    var numberRoundPage = '<div id="nrHTML">\
<p id="bigCards" hidden>25.50.75.100</p>\
<p id="littleCards" hidden>1.1.2.2.3.3.4.4.5.5.6.6.7.7.8.8.9.9.10.10</p>\
<h2>NUMBER ROUND</h2>\
<div id="numberBoard">\
<table class="nbTable" id="nbTable">\
<tr>\
<td></td>\
<td></td>\
<td class="targetCell" id="targetNum" colspan="2"></td>\
</tr>\
<tr id="chosenNumbers">\
<td class="emptyCell" id="cell0"></td>\
<td class="emptyCell" id="cell1"></td>\
<td class="emptyCell" id="cell2"></td>\
<td class="emptyCell" id="cell3"></td>\
<td class="emptyCell" id="cell4"></td>\
<td class="emptyCell" id="cell5"></td>\
</tr>\
</table>\
</div>\
<div id="numberBoardMessage">Add, subtract, multiply, and divide the numbers on the board to make the target number.  Each number may only be used once.  Answers are evaluated based on PEMDAS order of operations.</div>\
<form id="numberRoundForm" action="">\
<div id="pickerButtons"></div><br>\
<input class="form-control col-md-6" style="margin: auto;" id="numberRoundAnswer" type="text" placeholder="Enter Your Answer">\
</form>\
</div>\
<div id="gameMessages"></div>\
</div>';
    $('#wrapper').empty();
    $('#wrapper').append(numberRoundPage);
    //Add the scoreboard section to the page
    scoreBoard = '<div id="team1" class="col-md-3" style="float: left;"><div id="score0" style="font-size: 3em; color: white; font-weight: bold;">TEAM 1</div></div><div id="team2" class="col-md-3" style="float: right;"><div id="score1" style="font-size: 3em; color: white; font-weight: bold;">TEAM 2</div></div>';
    $('#wrapper').append(scoreBoard);
    team1Content = '';
    team2Content = '';
    $('#score0').append(" Score: " + workingCopy["score[]"][0]);
    $('#score1').append(" Score: " + workingCopy["score[]"][1]);
    for (var i = 0; i < workingCopy['users[]'].length; i++){
	if (i % 2 === 0){
	    team1Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
	else {
	    team2Content += '<div class="user_card" width="200px"><img class="card-img-top" src="/images/'+ randomUserImage(workingCopy['users[]'][i]) + '.jpg"><div>' + workingCopy['users[]'][i] + '</div></div>';
	}
    }
    $('#team1').append(team1Content);
    $('#team2').append(team2Content);

    //Do a request to get the list of games.  This runs twice for some reason.
    getGames().then(function(response){
	//console.log('run twice test');
	var game = getSpecificGame(key, response);
	//Set scoreboard
	//$('#score0').text(game["score[]"][0]);
	//$('#score1').text(game["score[]"][1]);
	//add the names of the users to the scoreboard.  ONLY WORKS WITH 2 USERS.
	//for (var i = 0; i < game['users[]'].length; i++){
	    //$('#name' + i).text(game['users[]'][i].toUpperCase());
	//};

	//Add big and little number buttons only for the picker.
	//console.log("picker: " + picker);
	//console.log("wcUser: " + game['users[]'][picker]);
	//console.log("user: " + currentUser);
	if (game['users[]'][picker] === currentUser){
	    //This if statement is a crappy bug fix for this code being run twice.  It checks if the pickerButtons div is empty before trying to add stuff to it.
	    if ($('#pickerButtons').is(':empty')){
		$('#pickerButtons').append('<p>Choose how many big and little numbers you would like</p>');
		$('#pickerButtons').append('<button class="btn btn-primary" type="button" id="btnChooseBig">Big</button><button class="btn btn-primary" type="button" id="btnChooseLittle">Little</button>');
	    }
	}
    });
	
};


//This function returns the leftmost empty square of the bottom row of the number board.
function getEmptySquaresNR(){
    var board = document.getElementById('nbTable');
    for (var i = 0; i < 6; i++){
	if (board.rows[1].cells[i].innerHTML === ''){
	    console.log('empty square is: ' + i);
	    return i;
	}
    };
};


//This function shouldn't have been created since we have hideLetterButtons so until we can update, this function calls that function.
function hideNumberButtons(){
    hideLetterButtons();
};


//This function takes a letter (String) and a position (String or Integer) as arguments.  It sets the cell in that position to the given letter.
function updateNumberBoard(number, position){
    if ($('#cell' + position.toString())){
	$('#cell' + position.toString()).text(number);
	$('#cell' + position.toString()).removeClass('emptyCell');
	$('#cell' + position.toString()).addClass('fullCell');
    }
};


//updates the numberboard target with the given number
function updateNumberBoardTarget(number){
    $('#targetNum').text(number);
    //$('#targetNum').removeClass('targetCell');
    $('#targetNum').addClass('fullTargetCell');
};


//Second version of function that returns a random little number
function getRandomLittleNumber(){
    cardStack = $('#littleCards').text().split('.');
    number = cardStack[Math.floor(Math.random() * cardStack.length)];
    cardStack.splice(cardStack.indexOf(number), 1);
    cardStack = cardStack.join('.');
    $('#littleCards').text(cardStack);
    return number;
};


//Second version of function that returns a random little number
function getRandomBigNumber(){ 
    cardStack = $('#bigCards').text().split('.');
    number = cardStack[Math.floor(Math.random() * cardStack.length)];
    cardStack.splice(cardStack.indexOf(number), 1);
    cardStack = cardStack.join('.');
    $('#bigCards').text(cardStack);
    return number;
};
   
	
    
//returns an integer (score). 
//Takes the player's equation (answer) and the random number (target)
function calculateScoreNR(answer, target){
    //exact match gets 10
    //being 5 away gets 7
    //being 6-10 away gets 5
    //being more than 10 gets 0
	
	/*//check that the answer is only math characters
	var regex = "^([-+]? ?(\d+|\(\g<1>\))( ?[-+*\/] ?\g<1>)?)$";
	if (!answer.match(regex)) {
    // alphabet letters found: 0 score
		return 0;
	}*/

    //if answer matches list, get score
    var guess = "unanswered";
    try{
	//try to set guess to the eval answer ... may fail
	guess = eval(answer);
    }catch(e){
	console.log(e);
    }
    if(guess != "unanswered"){
	var answerProximity = Math.abs(guess - target);
	if(answerProximity == 0){ return 10;}
	else if(answerProximity <= 5){ return 7;}
	else if(answerProximity <= 10){ return 5;}
	else{ return 0;}
    }else{
	//if guess is unanswered return score of 0;
	return 0;
    }
};


function replaceAll(string, search, replacement){
    return string.split(search).join(replacement);
}
	

//This function takes an equation (like 5+3*2) as input.  It returns true if the answer only uses numbers from the board and returns false if it uses other numbers.  
//Each number can only be used once.
function checkAnswerToBoard(answer){
    //strip non-number characters from answer and replace with a space
    answer = replaceAll(answer, "+", " ");
    answer = replaceAll(answer, "-", " ");
    answer = replaceAll(answer, "*", " ");
    answer = replaceAll(answer, "/", " ");
    answer = replaceAll(answer, ")", " ");
    answer = replaceAll(answer, "(", " ");
    
    console.log("answer with operators removed = " + answer);
	
    //grab numbers from board
    var board = new Array;
    board[0] = $('#cell0').text();
    board[1] = $('#cell1').text();
    board[2] = $('#cell2').text();
    board[3] = $('#cell3').text();
    board[4] = $('#cell4').text();
    board[5] = $('#cell5').text();
	
    console.log("Board = " + board);
	
    //break up the answer string into an array based on spaces
    answer = answer.split(' ');
    console.log("answer split on spaces = " + answer);
	
    //For each letter in the answer
    for (i = 0; i < answer.length; i++){
	letter = answer[i];
	//don't evaluate spaces
	if(letter == ""){
	    //do nothing
	    console.log("not evaluating this character, because it's a space");
	}else{
	    //if that letter is on the board, remove it from the board and move on
	    if (board.indexOf(letter) >= 0){
		board.splice(board.indexOf(letter), 1);
		console.log("The number " + letter + " is on the board");
	    }
	    //if it's not on the board return false
	    else {
		return false;
	    }
	}
    }
    //If all numbers were on the board return true
    return true;
};


//This function checks to see who won the letter round
//@returns string username : word : score
function highestScoreNR(){
    //Answer info is in the hiddenworkSpace paragraph.  Remove the first and last '.'
    var highest = ['Noone', 'No Answer', 0];
    var scoresInfo = $('#workSpace').text().slice(1);
    var scoresInfoByUser = scoresInfo.split('.');
    for (i = 0; i < scoresInfoByUser.length; i++){
	var userInfo = scoresInfoByUser[i].split(':');
	userInfo[2] = parseInt(userInfo[2], 10);
	if (userInfo[2] > highest[2]){
	    highest = userInfo;
	}
    }
    return highest[0] + ':' + highest[1] + ':' + highest[2].toString();
};


//This function get's a user's own score info (user, word, and potential score) for a LR
//returns string user - word - score
function getUserScoreInfoNR(){
    var user = $('head title').text().split(' - ')[1];
    var scoresInfo = $('#workSpace').text().slice(1);
    var scoresInfoByUser = scoresInfo.split('.');
    var userScore;
    for (var i = 0; i < scoresInfoByUser.length; i++ ){
	if (scoresInfoByUser[i].split(':')[0] === user){
	    return scoresInfoByUser[i];
	}
    }
};


//This function checks for a tie in the letter round results.  Only works if there's 2 players.
//Returns true if a tie or false if no tie
function checkTieNR(){
    scores = getScoresFromScoreboardLR();
	console.log("Score 0: " + scores[0]);
	console.log("Score 1: " + scores[1]);
    if (scores[0].split(':')[2] === scores[1].split(':')[2]){
		return true;
    }
    else{
		return false;
    }
};


//Heap's Algorithm for Generating Permutations https://en.wikipedia.org/wiki/Heap%27s_algorithm
function permute(array) {

    var permutations = [];
    generate(array);
    return permutations;

    function generate (array, n) {
	n = n || array.length;
	if (n == 1) {
	    permutations.push(array.slice());
	} else {
	    for (var i = 0; i < n - 1; i ++) {
		generate(array, n - 1);
		if (n % 2 === 0) {
		    swap(array, i, n-1);
		} else {
		    swap(array, 0, n-1)
		}
	    }
	    generate(array, n-1);
	}
    }

    function swap (array, a, b) {
	var temp = array[a];
	array[a] = array[b];
	array[b] = temp;
    }
}


//taken from https://stackoverflow.com/questions/9229645/remove-duplicates-from-javascript-array
function removeDuplicates(array) {
    var seen = {};
    return array.filter(function(item) {
	return seen.hasOwnProperty(item) ? false : (seen[item] = true);
    })
}


function solveNR(numberList, targetSolution) {
    var operations = ["+", "-", "*", "/"]
    var solutions = [];
    var allPermsOfList = permute(numberList);
    allPermsOfList = removeDuplicates(allPermsOfList);
    allPermsOfList.forEach(function(list) {
	solveRecursive(list, list[0].toString())
    });
    return solutions;

    function solveRecursive(numberList, testExpression) {
	if (numberList.length == 1) {
	    if (approximatelyEqual(numberList[0], targetSolution)) {
		solutions.push(testExpression);
	    }
	} else {
	    for(var operatorNum = 0; operatorNum < operations.length; operatorNum ++) {
		var newExpression = testExpression + operations[operatorNum] + numberList[1].toString();
		var collapsedList = collapseE1E2(numberList, operations[operatorNum]);
		solveRecursive(collapsedList, newExpression);
	    }
	}

    }

    function collapseE1E2(numberList, operation) {
	var copyArray = numberList.slice();
	var combined = copyArray[0];
	switch(operation) {
	case "+":
	    combined += copyArray[1];
	    break;
	case "-":
	    combined -= copyArray[1];
	    break;
	case "*":
	    combined *= copyArray[1];
	    break;
	case "/":
	    combined /= copyArray[1];
	    break;
	default:
	    throw("Unknown operation encountered during combination attempt!");
	}
	copyArray.splice(0, 2, combined);
	return copyArray;
    }

    function approximatelyEqual(n1, n2) {
	if(Math.abs(n1 - n2 ) < 0.00001) { //ish? sure.
	    return true;
	}
    }

}

