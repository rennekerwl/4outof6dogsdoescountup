$(function() {
    var socket = io('http://52.45.131.246:3001');
    $('#chatForm').submit(function() {
        socket.emit('chat message', $('#m').val());
        $('#m').val('');
        return false;
    });

    socket.on('chat message', function(msg) {
        $('#messages').append($('<li>').text(msg));
    });

    //Prevent form submission when enter is pressed for all forms
    $(document).on("keydown", "form", function(event) {
	return event.key != "Enter";
    });

    //When create game button is clicked emit a join game event with username and key
    $('#btnCreateGame').click(function() {
	var username = $('#createGameUserName').val();
	//Check that a username was entered.
	if (username){
	    generateKey();
	    socket.emit('join game', $('#createGameUserName').val(), $('head title').text().split(' - ')[0]);
	}
	return false;
    });

    //When join game button is clicked emit a join game event with username and key
    $('#btnJoinGame').click(function() {
	//Check for duplicate usernames before joining game.
	var key = $('#joinGame form input#inputGameKey').val().toUpperCase();
	var username = $('#joinGame form input#inputGameUserName').val();
	//Check for game key
	if (key){
	    //Check for username
	    if (username){
		getGames().then(function(games){
		    currentGame = games.find(function(element) {
			return element['key'] === key;
		    });
		    //If only one other user then userlist should be a string
		    if (Object.prototype.toString.call(currentGame['users[]']) === '[object String]') {
			//Check for duplicate username
			if (username.toUpperCase() != currentGame['users[]'].toUpperCase()){
			    socket.emit('join game', username, key);
			}
			else{
			    //alert("That username is already taken.  Please choose another.");
			}
		    }
		    //If more than one other users then userlist should be a list
		    else{
			//Check for duplicate username
			if (currentGame['users[]'].indexOf(username) === -1){
			    socket.emit('join game', username, key);
			}
			else{
			    //alert("That username is already taken.  Please choose another.");
			}
		    }
		});
	    }
	    else{
		//Alert for no username found.  Maybe this is done in global.js instead?
		//alert("Enter a username to continue.");
	    }
	}
	else {
	    //Alert for no key found.  Maybe this is done in global.js instead?
	    //alert("Enter a game key to continue.");
	}
        //socket.emit('join game', $('#inputGameUserName').val(), $('#inputGameKey').val().toUpperCase());
        return false;
    });

    //When start game button is clicked emit a start round event
    $(document).on('click', '#btnStartGame', function() {
        var key = $('head title').text().split(' - ')[0];
        socket.emit('start round', key);
        socket.emit('get word list', key);
        return false;
    });

    //When choose vowel button is clicked generate a random vowel.  Then emit an update lb event with that vowel and the leftmost empty square
    $(document).on('click', '#btnChooseVowel', function() {
        var key = $('head title').text().split(' - ')[0];
        var numVowels = countVowels();
        if (numVowels === 5) {
            $('#btnChooseVowel').attr('disabled', true);
            $('#gameMessages').append('<p>You cannot choose any more vowels.</p>');
        }
        if (numVowels < 5) {
            var vowel = getRandomLetter('vowel');
            var position = getEmptySquares();
            socket.emit('update lb', vowel, position, key);
            //Once the top half of the board is full hide the buttons
            if (getEmptySquares() === 8) {
                hideLetterButtons()
                socket.emit('start letterTimer', key);
            }
        }
        return false;
    });

    //When choose consonant button is clicked generate a random consonant.  Then emit an update lb event with that consonant and the leftmost empty square
    $(document).on('click', '#btnChooseConsonant', function() {
        var key = $('head title').text().split(' - ')[0];
        var numConsonants = countConsonants();
        if (numConsonants === 6) {
            $('#btnChooseConsonant').attr('disabled', true);
            $('#gameMessages').append('<p>You cannot choose any more consonants.</p>');
        }
        if (numConsonants < 6) {
            var consonant = getRandomLetter('consonant');
            var position = getEmptySquares();
            socket.emit('update lb', consonant, position, key);
            //Once the top half of the board is full hide the buttons
            if (getEmptySquares() === 8) {
                hideLetterButtons()
                socket.emit('start letterTimer', key);
            }
        }
        return false;
    });

    //When start next round button is clicked increment the current game's round by 1.  Then emit a start round socket event.
    $(document).on('click', '#btnStartNextRound', function() {
        var key = $('head title').text().split(' - ')[0];
        incrementRound().then(function(response) {
            socket.emit('start round', key);
        });
        return false;
    });

    //When a join game event occurs wait 1 second and then populate lobby with game's room
    socket.on('join game', function(userName, room) {
        //Add a delay because populateLobby is running before the DB gets updated sometimes.
        setTimeout(function() {
            populateLobby(room);
        }, 1000);
    });

    //When a start round event occurs run the start game function.  Then run the getGames function and emit an event for the appropriate round (letter, number, conundrum, etc.)
    socket.on('start round', function() {
        startGame();
        //log the current round to the console
        getGames().then(function(response) {
            console.log('round: ' + getRound(response));
        });

        //Get the current round of the current game and send out the appropriate socket event
        //This looks weird because it's async
        //For more info see: shor10.com/1Oj9H3
        getGames().then(function(response) {
            var key = $('head title').text().split(' - ')[0];
            if (getRound(response) === '0') {
                setTimeout(function() {
                    socket.emit('letter round', 0, key);
                }, 500);
            }
            if (getRound(response) === '1') {
                setTimeout(function() {
                    socket.emit('number round', 1, key);
                }, 500);
            }
            if (getRound(response) === '2') {
                setTimeout(function() {
                    socket.emit('letter round', 1, key);
                }, 500);
            }
            if (getRound(response) === '3') {
                setTimeout(function() {
                    socket.emit('number round', 0, key);
                }, 500);
            }
            if (getRound(response) === '4') {
                setTimeout(function() {
                    socket.emit('conundrum round', 1, key);
                }, 500);
            }
        });
    });

    //When a letter round event occurs, run the letterRound function with the picker as input
    socket.on('letter round', function(picker) {
        letterRound(picker);
    });

    //When a number round event occurs run the numberRound function with the picker as input
    socket.on('number round', function(picker) {
        numberRound(picker);
    });

    //When a conundrum round event occurs run the conundrumRound function
    socket.on('conundrum round', function(picker) {
        conundrumRound(picker);
    });

    //When start timer button is clicked add the conundrum to the board.  Then emit a start conundrum timer event
    $(document).on('click', '#btnStartTimer', function() {
        var key = $('head title').text().split(' - ')[0];
        getConundrum().then(function(response) {
            socket.emit('update cb', response, key);
            //hideConundrumButtons();
            socket.emit('start conundrumTimer', key);
        });
        return false;
    });

    //When start timer button is clicked add the conundrum to the board.  Then emit a start conundrum timer event
    $(document).on('click', '#btnShowFinal', function() {
        var key = $('head title').text().split(' - ')[0];
        socket.emit('show final', key);
        return false;
    });

    //When an update lb event occurs run the updateLetterBoard function with the received letter and position
    socket.on('update lb', function(letter, position) {
        updateLetterBoard(letter, position);
    });

    socket.on('start letterTimer', function() {
        var key = $('head title').text().split(' - ')[0];
        setTimeout(function() {
            //30 seconds after the letter round starts, add the answers to the workSpace paragraph

            var word = $('#letterRoundAnswer').val().toUpperCase().trim();
            var wordLength = word.length;
            var user = $('head title').text()
            var team = user.split(' - ')[2];
            user = user.split(' - ')[1];
            //Check that the word is valid for the board and is English.  Then add the score info to the hidden workSpace paragraph
            if (checkWordToBoard(word)) {
                console.log('sending word check request');
                socket.emit('word check', word, key);
                socket.on('word answer', function(wordReturned, answer) {
                    console.log('word checked: ' + wordReturned);
                    console.log('answer: ' + answer);
                    if (answer > -1) {
                        //answer is valid
                        if (wordLength < 9) {
                            //word length is less than 9, valid answer
                            var scoreInfo = '.' + user + '-' + word + '-' + wordLength + '-' + team + '.';
                        } else {
                            //word length is 9 or greater - double points
                            var scoreInfo = '.' + user + '-' + word + '-' + wordLength * 2 + '-' + team + '.';
                        }
                        getGames().then(function(response) {
                            if (getRound(response) === '0') {
                                console.log('English answer round 1, writing to workSpace: ' + scoreInfo);
                                console.log('Word: ' + word + ' Answer: ' + answer + ' wordReturned: ' + wordReturned);
                                //This if statement is a workaround because we get 2 answer responses and thus this code runs twice.
                                if (word === wordReturned) {
                                    socket.emit('update workspace lb', scoreInfo, key);
                                }
                            }
                            if (getRound(response) === '2') {
                                console.log('English answer round 2, writing to workSpace: ' + scoreInfo);
                                console.log('Word: ' + word + ' Answer: ' + answer + ' wordReturned: ' + wordReturned);
                                //This if statement is a workaround because we get 2 answer responses and thus this code runs twice.
                                if (word === wordReturned) {
                                    socket.emit('update workspace lb2', scoreInfo, key);
                                }
                            }
                        });
                    } else {
                        var scoreInfo = '.' + user + '-' + word + '-' + '0' + '-' + team +'.';
                        getGames().then(function(response) {
                            if (getRound(response) === '0') {
                                console.log('Not english answer round 1, writing to workSpace: ' + scoreInfo);
                                console.log('Word: ' + word + ' Answer: ' + answer + ' wordReturned: ' + wordReturned);
                                //This if statement is a workaround because we get 2 answer responses and thus this code runs twice.
                                if (word === wordReturned) {
                                    socket.emit('update workspace lb', scoreInfo, key);
                                }
                            }
                            if (getRound(response) === '2') {
                                console.log('Not english answer round 2, writing to workSpace: ' + scoreInfo);
                                console.log('Word: ' + word + ' Answer: ' + answer + ' wordReturned: ' + wordReturned);
                                //This if statement is a workaround because we get 2 answer responses and thus this code runs twice.
                                if (word === wordReturned) {
                                    socket.emit('update workspace lb2', scoreInfo, key);
                                }
                            }
                        });

                    }
                });
            } else {
                var scoreInfo = '.' + user + '-' + word + '-' + '0' + '-' + team + '.'
                getGames().then(function(response) {
                    if (getRound(response) === '0') {
                        console.log('Doesnt fit board round 1, writing to workSpace: ' + scoreInfo);
                        socket.emit('update workspace lb', scoreInfo, key);
                    } else if (getRound(response) === '2') {
                        console.log('Doesnt fit board round 2, writing to workSpace: ' + scoreInfo);
                        socket.emit('update workspace lb2', scoreInfo, key);
                    }
                });
		
            }
            setTimeout(function() { //Added a 1 second timeout so that the previous socket code can all run before we run this.
                //This code updates the score in the DB.  Each user should update their own score if they win.
                if (checkForAnswersLR()) {
                    console.log('running score updates')
                    //If there are answers, get this user's score info
                    userScoreInfo = getUserScoreInfoLR();
                    if (checkTieLR()) {
                        console.log('There was a tie');
                        //If it's a tie, update both user's scores by the score amount from the first user.
			getGames().then(function(games){
			    currentGame = games.find(function(element){
				return element['key'] === key;
			    });
			    if (user === currentGame['users[]'][0]){
				scoreInfos = getScoresFromScoreboardLR();
				updateScoreTie(parseInt(scoreInfos[0].split('-')[2], 10));
			    }
			});
                    } else {
                        console.log('there was not a tie')
                        if (highestScoreLR() === userScoreInfo) {
                            console.log('we won, updating score')
                            //If this user's score is the highest, update this user's score
                            updateScore(userScoreInfo.split('-')[0], parseInt(userScoreInfo.split('-')[2], 10));
                        }
                    }
                }
            }, 1000);
            $('#gameMessages').append('<p>Time up!</p>');
            socket.emit('display lr results', key);
	    
        }, 30000);
	
    });

    //When an update workspace lb event is received, add the score info string to the hidden workspace paragraph.
    socket.on('update workspace lb', function(textToUpdate) {
        $('#workSpace').text($('#workSpace').text() + textToUpdate);
        console.log('workspace round 0 updated.');
    });
    
    //When an update workspace lb 2 event is received, add the score info string to the hidden workspace paragraph.
    socket.on('update workspace lb2', function(textToUpdate) {
        //$('#workSpaceRound2').text($('#workSpaceRound2').text() + textToUpdate);
        $('#workSpace').text($('#workSpace').text() + textToUpdate);
        console.log('workspace round 2 updated.');
    });

    //When an update workspace nb event is received, add the score info string to the hidden workspace paragraph.
    socket.on('update workspace nb', function(textToUpdate) {
        $('#workSpace').text($('#workSpace').text() + textToUpdate);
    });
    
    socket.on('display lr results', function() {
        //Start a 3 second timer to wait for scores to update then display the score info
        setTimeout(function() {
            console.log('Time to display LR results');
            var key = $('head title').text().split(' - ')[0];
            var user = $('head title').text().split(' - ')[1];
            var board = getBoardLR();
            var scores = getScoresFromScoreboardLR();
            getGames().then(function(response) {
                var game = getSpecificGame(key, response);
		for (i = 0; i < scores.length; i++){
		    var message = '<p>' + scores[i].split('-')[0] + ' got ' + scores[i].split('-')[1] + ' a ' + scores[i].split('-')[2] + ' letter word!</p>';
                    $('#gameMessages').append(message);
		}
                //Add a next round button only for the first player.
                if (user === game['users[]'][0]) {
                    $('#wrapper').append('<form id="startNextRound" action=""><button class="btn btn-primary" type="button" id="btnStartNextRound">Start Next Round</button></form>');
                }
		
                //socket.emit('get longest word', board, key);
		//Commenting this out for performance reasons
		//getLongestWord(board);
		
            });
        }, 3000);
    });
    
    /*MOVED TO CLIENT-SIDE FUNCTION
      socket.on('found longest word', function(word) {
      var message2 = '<p>The longest word we could find in Dictionary Corner is ' + word + ' which is a ' + word.length + ' letter word!</p>';
      $('#gameMessages').append(message2);
      
      });*/
    
    //When choose big button is clicked generate a random big number.  Then emit an update nb event with that number and the leftmost empty square
    $(document).on('click', '#btnChooseBig', function() {
        var key = $('head title').text().split(' - ')[0];
        //Check to make sure there are big numbers left
        if ($('#bigCards').is(':empty')) {
            $('#gameMessages').append('<p>You cannot choose any more big numbers.</p>');
            $('#btnChooseBig').attr('disabled', true);
        } else {
            var big = getRandomBigNumber();
            var position = getEmptySquaresNR();
            socket.emit('update nb', big, position, key);
            //Once the top half of the board is full hide the buttons
            if (getEmptySquaresNR() === 5) {
                hideNumberButtons()
		
                //get a random number between 1 and 999 as our target;
                var cecil = Math.floor(Math.random() * 999); //our target number
		
                //broadcast target number
                socket.emit('set target number', cecil, key);
		
                socket.emit('start numberTimer', key);
            }
        }
        return false;
    });
    
    //When choose little button is clicked generate a random little number.  Then emit an update nb event with that number and the leftmost empty square
    $(document).on('click', '#btnChooseLittle', function() {
        var key = $('head title').text().split(' - ')[0];
        var little = getRandomLittleNumber();
        var position = getEmptySquaresNR();
        socket.emit('update nb', little, position, key);
        //Once the top half of the board is full hide the buttons
        if (getEmptySquaresNR() === 5) {
            hideNumberButtons()
	    
            //get a random number between 1 and 999 as our target;
            var cecil = Math.floor(Math.random() * 999); //our target number
	    
            //broadcast target number
            socket.emit('set target number', cecil, key);
	    
            socket.emit('start numberTimer', key);
        }
        return false;
    });
    
    //Conundrum round Submit answer button
    $(document).on('click', '#btnConundrumSubmit', function() {
        var key = $('head title').text().split(' - ')[0];
        //socket.emit('start round');
        if ($('#conundrumRoundAnswer').val().toUpperCase().trim() === $('#workSpace').text()) {
            //the answers is correct
            var user = $('head title').text().split(' - ')[1];
            updateScore(user, 10);
            socket.emit('display cr results', user, key);
        } else {
            //answer is incorrect
            $('#btnConundrumSubmit').attr("disabled", true); //disable submit button
            $('#conundrumRoundAnswer').attr("disabled", true); //disable answer field
            $('#gameMessages').append('Sorry, that was not the correct answer');
        }
	
        return false;
    });
    
    //set the target number for the number round
    socket.on('set target number', function(target) {
        updateNumberBoardTarget(target);
    });
    
    //When an update nb event occurs run the updateNumberBoard function with the received number and position
    socket.on('update nb', function(number, position) {
        updateNumberBoard(number, position);
    });
    
    socket.on('start numberTimer', function() {
        console.log("numberTimer started");
        var key = $('head title').text().split(' - ')[0];
        setTimeout(function() {
            //30 seconds after the letter round starts, add the answers to the workSpace paragraph
	    
            var target = $('#targetNum').text();
            var answer = $('#numberRoundAnswer').val().trim();
            
            //validate that our answer is just numbers
            var regex = /([1234567890\(\)\+\-\*\s\/])/g;
            if (!answer.match(regex)) {
                //non math characters found, set answer to undefined
                answer = "undefined";
            }	
            
            var score = calculateScoreNR(answer, target);
            var user = $('head title').text()
            user = user.split(' - ')[1];
	    
            //Check that the answer is valid for the board and is calculate points.  Then add the score info to the hidden workSpace paragraph
            if (checkAnswerToBoard(answer)) {
                //check if the answer evaluates
                try {
                    if (eval(answer) == 'undefined') { //answer does not evaluate
                        var scoreInfo = '.' + user + ':' + 'no answer received' + ':' + score;
                    } else { //answer evaluates, give score
                        var scoreInfo = '.' + user + ':' + answer + '=' + eval(answer) + ':' + score;
                    }
                } catch (e) {
                    //trying to evaluate the answer gives an error
                    console.log('Answer does not computer - 0 score');
                    var scoreInfo = '.' + user + ':' + 'no answer received' + ':' + 0;
                }
            } else { //answer does not use numbers from board
                console.log("Answer does not use numbers from the board");
                var scoreInfo = '.' + user + ':' + answer + ':' + 0;
            }
            socket.emit('update workspace nb', scoreInfo, key);
	    
            setTimeout(function() { //Added a 1 second timeout so that the previous socket code can all run before we run this.
                //This code updates the score in the DB.  Each user should update their own score if they win.
                if (checkForAnswersLR()) {
                    console.log('running score updates')
                    //If there are answers, get this user's score info
                    userScoreInfo = getUserScoreInfoNR();
                    if (checkTieNR()) {
                        console.log('There was a tie with: ' + userScoreInfo);
                        //If it's a tie, update both user's scores by the score amount from the first user.
			getGames().then(function(games){
			    currentGame = games.find(function(element){
				return element['key'] === key;
			    });
			    if (user === currentGame['users[]'][0]){
				scoreInfos = getScoresFromScoreboardLR();
				updateScoreTie(parseInt(scoreInfos[0].split(':')[2], 10));
			    }
			});
                    } else {
                        console.log('there was not a tie')
                        if (highestScoreNR() === userScoreInfo) {
                            console.log('we won, updating score')
                            //If this user's score is the highest, update this user's score
                            updateScore(userScoreInfo.split(':')[0], parseInt(userScoreInfo.split(':')[2], 10));
                        }
                    }
                }
            }, 1000);
            $('#gameMessages').append('<p>Time up!</p>');
            socket.emit('display nr results', key);
	    
        }, 30000);
	
    });
    
    socket.on('display nr results', function() {
        //Start a 3 second timer to wait for scores to update then display the score info
        setTimeout(function() {
            console.log('Time to display NR results');
            var key = $('head title').text().split(' - ')[0];
            var user = $('head title').text().split(' - ')[1];
            var scores = getScoresFromScoreboardNR();
            getGames().then(function(response) {
                var game = getSpecificGame(key, response);
                var message0 = '<p>' + scores[0].split(':')[0] + ' got ' + scores[0].split(':')[1] + ' for ' + scores[0].split(':')[2] + ' points!</p>';
                $('#gameMessages').append(message0);
                var message1 = '<p>' + scores[1].split(':')[0] + ' got ' + scores[1].split(':')[1] + ' for ' + scores[1].split(':')[2] + ' points!</p>';
                $('#gameMessages').append(message1);
                //Update the scoreboard for player 1
                //$('#score0').text(game['score[]'][game['users[]'].indexOf(scores[0].split(':')[0])], 10);
                //Update the scoreboard for player 2
                //$('#score1').text(parseInt($('#score1').text(), 10) + parseInt(scores[1].split(':')[2], 10));
                //Add a next round button only for the first player.
                if (user === game['users[]'][1]) {
                    $('#wrapper').append('<form id="startNextRound" action=""><button class="btn btn-primary" type="button" id="btnStartNextRound">Start Next Round</button></form>');
                }
		
                socket.emit('show nr solution', key);
            });
        }, 3000);
        //socket.emit('show number solution');
    });
    
    socket.on('show nr solution', function() {
        //Start a 2 second timer to wait for scores to update then display the score info
        setTimeout(function() {
            console.log('Display NR solution');
            var board = getBoardNR();
            var target = parseInt($('#targetNum').text(), 10);
            var answers = solveNR(board, target);
            var nrSolutionMessage = '<p>The solution we found (solving from left to right) was ' + answers[0] + '</p>';
            $('#gameMessages').append(nrSolutionMessage);
        }, 3000);
    });
    
    var conundrumTimer = -1;
    socket.on('start conundrumTimer', function() {
        //enable submit button by removing the disabled attribute
        $('#btnConundrumSubmit').attr("disabled", false); //enable submit button
        
        conundrumTimer = setTimeout(function() {
            //30 seconds after the letter round starts, add the answers to the workSpace paragraph
	    
            var user = $('head title').text();
            user = user.split(' - ')[1];
            $('#gameMessages').append('Time up!');
	    
            //disable submit
            $('#btnConundrumSubmit').attr("disabled", true); //disable submit button
            $('#conundrumRoundAnswer').attr("disabled", true); //disable answer field
	    
            $('#gameMessages').append('<br>The correct answer was: ' + $("#workSpace").text());
            $('#wrapper').append('<button class="btn btn-primary" type="button" id="btnShowFinal">Show Final Score</button>');
        }, 30000);
	
    });
    
    //When an update cb event occurs run the updateConundrumBoard function with the w
    socket.on('update cb', function(conundrumData) {
	console.log('update cb event received.');
        drawConundrum(conundrumData);
    });
    
    //show conundrum results
    socket.on('display cr results', function(user) {
        console.log("Cancelling Timer. Timer = " + conundrumTimer);
        clearTimeout(conundrumTimer);
	
        //disable submit
        $('#btnConundrumSubmit').attr("disabled", true); //disable submit button
        $('#conundrumRoundAnswer').attr("disabled", true); //disable answer field
	
        var ans = $("#workSpace").text();
        $('#gameMessages').append(user + ' answered correctly with, ' + ans);
        //Add show final results button
        $('#wrapper').append('<button class="btn btn-primary" type="button" id="btnShowFinal">Show Final Score</button>');
    });
    
    //show final results
    socket.on('show final', function() {
        resultsRound();
    });
    
    var wordSet;
    socket.on('get word list', function(list) {
        console.log("Getting wordSet");
        console.log(list);
	wordSet = new Set(list);
    });
    
    
    //Gets the longest word given the board and a list of words
    function getLongestWord(board) {
        combos = tree(board.split('')).map(function(str) {
            return str.join('')
        });
        combosUnique = uniq(combos);
        combosUnique.sort(function(a, b) {
            return b.length - a.length;
        });
        for (var i = 0; i < combosUnique.length; i++) {
            word = combosUnique[i];
	    
            isWord = wordSet.has(word.toLowerCase());
            if (isWord === true) {
                //socket.emit('found longest word', word);
		var message2 = '<p>The longest word we could find in Dictionary Corner is ' + word + ' which is a ' + word.length + ' letter word!</p>';
		$('#gameMessages').append(message2);
                break;
            }
        }
    };
    
    /*
     *Takes an array and filters out duplicates
     *so that each array element is unique
     *@param a array
     *@returns array without duplicate
     */
    function uniq(a) {
        var seen = {};
        return a.filter(function(item) {
            return seen.hasOwnProperty(item) ? false : (seen[item] = true);
        });
    }
    
    
    //Used like combos = tree('fisherman'.split('').map(function(str){return str.join('')});
    /*
     *Used for letter round to detrmine all combinations of the input string
     *@param leafs a string letters
     *@returns every permutation of that combination of letters
     */
    var tree = function(leafs) {
	var branches = [];
	if (leafs.length == 1) return leafs;
	for (var k in leafs) {
	    var leaf = leafs[k];
	    tree(leafs.join('').replace(leaf, '').split('')).concat("").map(function(subtree) {
		branches.push([leaf].concat(subtree));
	    });
	}
	return branches;
    };
    
});
