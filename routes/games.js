var express = require('express');
var router = express.Router();

/* GET /games/games */
router.get('/games', function(req,res) {
    var db = req.db; //get the DB
    var collection = db.get('games'); //get the games collection (table)
    collection.find({},{},function(e,docs){ //return all documents (rows) in the collection
	res.json(docs);
    });
});

/* POST to /games/addgame */
//request body contains full JSON object of new game info
router.post('/addgame', function(req, res) {
    var db = req.db;
    var collection = db.get('games');
    collection.insert(req.body, function(err, result){
	res.send(
	    (err === null) ? {msg: ''} : {msg: err}
	);
    });
});

/* DELETE to /games/deletegame */
//Untested but should work
router.delete('/deletegame/:id', function(req, res) {
    var db = req.db;
    var collection = db.get('games');
    var gameToDelete = req.params.id;
    collection.remove({ '_id' : gameToDelete }, function(err) {
	res.send((err === null) ? { msg: '' } : { msg:'error: ' + err});
    });
});

/* PUT to /games/putgame */
//request body contains full JSON object of updated game info
router.put('/putgame/:id', function(req,res) {
    var db = req.db;
    var collection = db.get('games');
    var gameToUpdate = req.params.id;
    collection.update({ '_id': gameToUpdate }, req.body, function(err) {
	res.send((err === null) ? {msg: ''} : {msg: 'error: ' + err});
    });
});
	   
/*test for variables in /games/testVar*/
router.get('/:testVar', function(req,res) {
    if (req.params['testVar'] === 'ABCD'){
	res.send('Welcome to game ABCD!');
    };
});

module.exports = router;
