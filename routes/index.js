var express = require('express');
var router = express.Router();
var path = require('path');


/* GET home page. */
//Serves jade template
//router.get('/', function(req, res, next) {
//    res.render('index', { title: 'Express' });
//});

/* GET home page. */
//Serves static html
router.get('/', function(req, res, next) {
    res.sendFile(path.join(__dirname + '/../views/index.html'));
});

module.exports = router;
